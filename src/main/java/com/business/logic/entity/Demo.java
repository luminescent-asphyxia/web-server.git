package com.business.logic.entity;

import com.server.utils.JsonUtil;

import java.util.*;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/30 14:06
 */
public class Demo {
    private long aLong;
    private int aInt;
    public short aShort;
    private static boolean isAlive;
    private String name;
    private byte[] bytes;
    private Date[] dates;
    private int[][][] superIntArr;
    private String[][] strings;
    private List<Map<Integer,String>> list;
    private Set<int[]> set;
    private Map<String,List<int[]>> map;

    public static void main(String[] args) throws IllegalAccessException {
        Demo demo = Demo.getObj();
        String json = JsonUtil.toJsonStr(demo);
        System.out.println(json);
    }

    public static Demo getObj(){
        Demo demo = new Demo();
        demo.setaLong(1000012L);
        demo.setaInt(120);
        Demo.setIsAlive(true);
        demo.setaShort((short)78);
        demo.setName("Demo Test");
        byte[] bytes = new byte[2];
        bytes[0] = 1;
        bytes[1] = 127;
        demo.setBytes(bytes);
        Date[] dates = new Date[3];
        dates[1] = new Date();
        demo.setDates(dates);
        int[][][] superIntArr = new int[2][2][2];
        for (int i = 0;i<superIntArr.length;i++){
            for (int j = 0;j<superIntArr[i].length;j++){
                for (int k = 0;k<superIntArr[j].length;k++){
                    superIntArr[i][j][k] = (int)(Math.random()*100);
                }
            }
        }
        demo.setSuperIntArr(superIntArr);
        String[][] strings = new String[5][2];
        for (int i = 0; i < strings.length; i++) {
            for (int j = 0; j < strings[i].length; j++) {
                strings[i][j] = String.valueOf(Math.random());
            }
        }
        demo.setStrings(strings);
        List<Map<Integer,String>> list = new LinkedList<>();
        Map<Integer,String> map = new Hashtable<>();
        map.put(1,"demo1");
        map.put(2,"demo2");
        map.put(3,"demo3");
        Map<Integer,String> map2 = new Hashtable<>();
        map2.put(12,"demo12");
        map2.put(22,"demo22");
        map2.put(32,"demo32");
        list.add(map);
        list.add(map2);
        demo.setList(list);
        Set<int[]> set = new HashSet<>();
        for (int i = 0;i<5;i++){
            int len = (int)(Math.random()*10)+2;
            int[] tempArr = new int[len];
            for (int j = 0; j < tempArr.length; j++) {
                tempArr[j] = 100 - j;
            }
            set.add(tempArr);
        }
        demo.setSet(set);
        Map<String,List<int[]>> maps = new LinkedHashMap<>();
        for (int i = 0;i<=3;i++){
            List<int[]> tempList = new Stack<>();
            for (int j = 20;j>=0;j--){
                int[] hh = new int[5];
                for (int k = 0; k < hh.length; k++) {
                    hh[k] = k+j;
                }
                tempList.add(hh);
            }
            maps.put("list"+i,tempList);
        }
        demo.setMap(maps);
        return demo;
    }

    public static void setIsAlive(boolean isAlive) {
        Demo.isAlive = isAlive;
    }

    public void setaLong(long aLong) {
        this.aLong = aLong;
    }

    public void setaInt(int aInt) {
        this.aInt = aInt;
    }

    public void setaShort(short aShort) {
        this.aShort = aShort;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public void setDates(Date[] dates) {
        this.dates = dates;
    }

    public void setSuperIntArr(int[][][] superIntArr) {
        this.superIntArr = superIntArr;
    }

    public void setStrings(String[][] strings) {
        this.strings = strings;
    }

    public void setList(List<Map<Integer, String>> list) {
        this.list = list;
    }

    public void setSet(Set<int[]> set) {
        this.set = set;
    }

    public void setMap(Map<String, List<int[]>> map) {
        this.map = map;
    }
}
