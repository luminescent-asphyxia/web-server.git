package com.server.web.annotation;

import java.lang.annotation.*;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/27 16:17
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebServlet {
    /**
     * 指定Servlet 的 name 属性，等价于 <servlet-name>。如果没有显式指定，则该 Servlet 的取值即为类的全限定名。
     */
    String name() default "";

    /**
     * 根据Servlet官方的规则：该属性等价于 urlPatterns 属性。并且两个属性不能同时使用。
     * 这里模仿该规则的实现
     */
    String[] value() default {};

    /**
     * 指定一组 Servlet 的 URL 匹配模式。等价于<url-pattern>标签。
     */
    String[] urlPatterns() default {};
}
