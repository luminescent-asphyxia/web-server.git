package com.server.utils;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/10 12:18
 */
public class SecurityClose {
    public static void close(AutoCloseable... autoCloseables) {
        for (AutoCloseable autoCloseable : autoCloseables) {
            try {
                if (autoCloseable != null)
                    autoCloseable.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
