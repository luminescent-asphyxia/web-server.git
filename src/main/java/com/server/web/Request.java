package com.server.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.*;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/8 10:21
 */
public class Request {

    private String requestInfo;

    private String method;

    private String url;

    private Map<String, List<String>> parameters;

    private String parameterStr = "";

    public Request(Socket client) throws IOException {
        InputStream in = client.getInputStream();
        byte[] data = new byte[1024 * 1024];
        int len = 0;
        len = in.read(data);
        if (len>0){
            this.requestInfo = new String(data, 0, len);
            this.parseRequestInfo();
            this.parseParameterStr();
        }
    }

    private void parseRequestInfo() {
        int firstIndex = requestInfo.indexOf("/");
        this.method = requestInfo.substring(0, firstIndex - 1).toLowerCase();
        int httpIndex = requestInfo.indexOf("HTTP");
        String tempUrl = requestInfo.substring(firstIndex, httpIndex - 1);
        int parameterIndex = tempUrl.indexOf("?");
        if (parameterIndex == -1) {
            this.url = tempUrl;
        } else {
            this.url = tempUrl.substring(0, parameterIndex);
            this.parameterStr += tempUrl.substring(parameterIndex + 1);
            //只有get方式的参数有可能乱码，post不用做处理
            this.parameterStr = decode(parameterStr, "UTF-8");
        }
        this.url =decode(this.url,"UTF-8");
        if ("post".equals(method)) {
            String crlf = "\r\n";
            int index = requestInfo.lastIndexOf(crlf);
            index += crlf.length();
            if (this.parameterStr.length() > 0) {
                this.parameterStr += "&";
            }
            this.parameterStr += requestInfo.substring(index);
        }
    }

    private void parseParameterStr() {
        String[] parameterArr = parameterStr.split("&");
        if (parameterArr.length > 0) {
            parameters = new HashMap<>();
            for (String p : parameterArr) {
                String[] kvs = p.split("=");
                kvs = Arrays.copyOf(kvs, 2);
                String k = kvs[0];
                String v = kvs[1];
                if (k == null || v == null) {
                    continue;
                }
                if (!parameters.containsKey(k)) {
                    parameters.put(k, new ArrayList<>());
                }
                parameters.get(k).add(v);
            }
        }
    }

    public String getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public Map<String, List<String>> getParameterMap() {
        return parameters;
    }

    public String[] getParameterValues(String key) {
        List<String> list = parameters.get(key);
        if (list == null || list.size() == 0) {
            return null;
        } else {
            return list.toArray(new String[0]);
        }
    }

    public String getParameter(String key) {
        String[] values = this.getParameterValues(key);
        return values == null ? null : values[0];
    }

    private String decode(String value, String enc) {
        try {
            return java.net.URLDecoder.decode(value, enc);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
