package com.server.web.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/7 15:29
 */

public class Mapping {
    private String name;

    private Set<String> urlPatterns = new HashSet<>();

    public Mapping(){}

    public Mapping(String name, Set<String> urlPatterns) {
        this.name = name;
        this.urlPatterns = urlPatterns;
    }

    public boolean addPattern(String pattern){
        return urlPatterns.add(pattern);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getUrlPatterns() {
        return urlPatterns;
    }

    public void setUrlPatterns(Set<String> urlPatterns) {
        this.urlPatterns = urlPatterns;
    }
}
