package com.server.web;

import com.server.utils.SecurityClose;
import com.server.utils.logger.Log;
import com.server.utils.logger.LogImpl;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/7 17:05
 */
public class Server {
    private ServerSocket serverSocket;

    private volatile boolean isRunning = false;

    private Log log = LogImpl.getLogger();

    private int serverPort = ServerConfig.getServerPort();

    private static ThreadPoolExecutor tpe;

    public static final String serverName = "QaServer/1.1";

    static {
        ServerConfig.init();
        WebApplication.init();
        tpe = new ThreadPoolExecutor(ServerConfig.getCorePoolSize(),
                ServerConfig.getMaximumPoolSize(),
                ServerConfig.getKeepAliveTime(),
                ServerConfig.getTimeUnit(),
                new LinkedBlockingDeque<>(5));
    }

    public void start() {
        try {
            log.debug("服务器正在启动...");
            serverSocket = new ServerSocket(serverPort);
            isRunning = true;
            log.debug("web.xml所在位置："+ServerConfig.getWebXmlLocation());
            if (ServerConfig.isPrintStaticRequestLog()){
                log.debug("已开启静态资源请求日志打印");
            }
            if (ServerConfig.isAnnotationSupport()){
                log.debug("已开启注解支持");
                log.debug("注解所在的包："+ServerConfig.getAnnotationPackage());
            }
            log.debug("线程核心池的大小："+ServerConfig.getCorePoolSize());
            log.debug("线程池最大线程数："+ServerConfig.getMaximumPoolSize());
            log.debug("线程最大空闲时间："+ServerConfig.getKeepAliveTime()+ServerConfig.getTimeUnit().toString().toLowerCase());
            log.debug("Java虚拟机开辟的内存空间大小为："+Runtime.getRuntime().maxMemory()/1024/1024+"M");
            log.debug("当前程序已占用内存："+Runtime.getRuntime().totalMemory()/1024/1024+"M");
            log.debug("服务器启动成功，正在监听端口：" + serverPort);
            this.receive();
        } catch (IOException e) {
            e.printStackTrace();
            log.debug("服务器启动失败！");
            this.stop();
        }
    }

    public void receive() {
        while (isRunning) {
            try {
                Socket client = serverSocket.accept();
                tpe.execute(new Dispatcher(client));
            } catch (IOException e) {
                e.printStackTrace();
                log.debug("客户端错误..." + e.getMessage());
            }
        }
    }

    public void stop() {
        isRunning = false;
        SecurityClose.close(this.serverSocket);
        log.debug("服务器已停止");
    }
}
