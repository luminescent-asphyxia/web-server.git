package com.server.web;

import com.server.web.entity.Entity;
import com.server.web.entity.Mapping;
import com.server.web.entity.Resources;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/7 15:24
 */
public class WebHandler extends DefaultHandler {

    private List<Entity> entityList;

    private List<Mapping> mappingList;

    private List<Resources> resourcesList;

    private Entity entity;

    private Mapping mapping;

    private Resources resources;

    private String currentTag;

    private int tagFlag;//{1：servlet，2：servletMapping，3：resources}


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName){
            case "servlet":
                if (entityList == null){
                    entityList = new ArrayList<>();
                }
                entity = new Entity();
                tagFlag = 1;
                break;
            case "servlet-mapping":
                if (mappingList == null){
                    mappingList = new ArrayList<>();
                }
                mapping = new Mapping();
                tagFlag = 2;
                break;
            case "resources":
                if (resourcesList == null){
                    resourcesList = new ArrayList<>();
                }
                resources = new Resources();
                tagFlag = 3;
                break;
        }
        currentTag = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName){
            case "servlet":
                entityList.add(entity);
                break;
            case "servlet-mapping":
                mappingList.add(mapping);
                break;
            case "resources":
                resourcesList.add(resources);
        }
        currentTag = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String content = new String(ch,start,length).trim();
        if (content.length()>0){
            switch (tagFlag){
                case 1:
                    if ("servlet-name".equals(currentTag)){
                        entity.setName(content);
                    } else if ("servlet-class".equals(currentTag)){
                        entity.setClazz(content);
                    }
                    break;
                case 2:
                    if ("servlet-name".equals(currentTag)){
                        mapping.setName(content);
                    } else if ("url-pattern".equals(currentTag)){
                        mapping.addPattern(content);
                    }
                    break;
                case 3:
                    if ("resource-directory".equals(currentTag)){
                        resources.setDirectory(content);
                    } else if ("resource-pattern".equals(currentTag)){
                        resources.addPattern(content);
                    }
            }
        }
    }

    public List<Entity> getServletEntities() {
        return entityList;
    }

    public List<Mapping> getServletMappingEntities() {
        return mappingList;
    }

    public Set<Resources> getResourcesList() {
        return new HashSet<>(resourcesList);
    }
}
