package com.business.logic;

import com.server.web.Request;
import com.server.web.Response;
import com.server.web.servlet.HttpServlet;
import com.server.web.servlet.Servlet;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/7 16:34
 */
public class RegisterServlet extends HttpServlet {

    @Override
    public void service(Request request, Response response) {
        response.println("恭喜，注册成功！！！");
    }
}
