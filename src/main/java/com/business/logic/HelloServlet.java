package com.business.logic;

import com.server.web.servlet.HttpServlet;
import com.server.web.Request;
import com.server.web.Response;
import com.server.web.annotation.WebServlet;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/26 15:12
 */
@WebServlet("/hello")
public class HelloServlet extends HttpServlet {

    @Override
    public void service(Request request, Response response) throws Exception {
        response.println("Hello，Spring Boot2");
    }

    @Override
    public void doGet(Request request, Response response) throws Exception {
        response.println("Hello，world!!");
    }
}
