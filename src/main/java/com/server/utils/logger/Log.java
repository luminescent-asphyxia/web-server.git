package com.server.utils.logger;

public interface Log{
	//记录debug级别的日志
	public void debug(String message);
	//记录info级别的日志
	public void info(String message);
	//记录warn级别的日志
	public void warn(String message);
	//记录error级别的日志
	public void error(String message);
	//记录fatal级别的日志
	public void fatal(String message);
}