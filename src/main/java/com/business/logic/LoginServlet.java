package com.business.logic;

import com.server.web.Request;
import com.server.web.Response;
import com.server.web.servlet.Servlet;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/7 16:33
 */
public class LoginServlet implements Servlet {

    @Override
    public void service(Request request, Response response) {
        response.print("<html>");
        response.print("<head>");
        response.print("<title>");
        response.print("登录页面");
        response.print("</title>");
        response.print("</head>");
        response.print("<body>");
        response.print("<h1>");
        response.print("hello login!!！ "+request.getParameter("name"));
        response.print("</h1>");
        response.print("</body>");
        response.print("</html>");
    }
}
