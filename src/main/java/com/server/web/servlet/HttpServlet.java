package com.server.web.servlet;

import com.server.web.Request;
import com.server.web.Response;

/**
 * @author qiaoao
 * @description:
 * @date 2021/8/1 14:34
 */
public class HttpServlet extends GenericServlet {
    @Override
    public void doGet(Request request, Response response) throws Exception {
        throw new UnsupportedOperationException();
    }

    @Override
    public void doPost(Request request, Response response) throws Exception {
        throw new UnsupportedOperationException();
    }

    @Override
    public void service(Request request, Response response) throws Exception {
        throw new UnsupportedOperationException();
    }
}
