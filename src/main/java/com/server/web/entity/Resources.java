package com.server.web.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/10 12:40
 */

public class Resources {
    private String directory;
    private Set<String> patterns = new HashSet<>();

    public Resources(){}

    public Resources(String directory, Set<String> patterns) {
        this.directory = directory;
        this.patterns = patterns;
    }

    public boolean addPattern(String pattern){
        return patterns.add(pattern);
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public Set<String> getPatterns() {
        return patterns;
    }

    public void setPatterns(Set<String> patterns) {
        this.patterns = patterns;
    }
}
