package com.server.web;

import com.server.web.entity.Entity;
import com.server.web.entity.Mapping;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/7 16:21
 */
public class WebContext {

    //key:name value:clazz
    private Map<String,String> entityMap;

    //key:urlPattern value:name
    private Map<String,String> mappingMap;

    public WebContext(List<Entity> entityList, List<Mapping> mappingList){
        entityMap = new HashMap<>();
        mappingMap = new HashMap<>();
        for (Entity entity : entityList) {
            entityMap.put(entity.getName(),entity.getClazz());
        }
        for (Mapping mapping : mappingList) {
            Set<String> urlPatterns = mapping.getUrlPatterns();
            for (String urlPattern : urlPatterns) {
                mappingMap.put(urlPattern,mapping.getName());
            }
        }
    }

    public Class<?> getClazz(String urlPattern) throws Exception {
        String name = mappingMap.get(urlPattern);
        String clz = entityMap.get(name);
        if (clz == null){
            //说明路径找不到，应该报404错误
            return null;
        }
        return Class.forName(clz);
    }
}
