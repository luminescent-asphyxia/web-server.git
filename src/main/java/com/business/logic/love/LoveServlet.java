package com.business.logic.love;

import com.server.web.Request;
import com.server.web.Response;
import com.server.web.servlet.Servlet;
import com.server.web.annotation.WebServlet;
import com.business.logic.entity.Demo;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/30 14:42
 */
@WebServlet("/love")
public class LoveServlet implements Servlet {

    @Override
    public void service(Request request, Response response) throws Exception {
        Demo obj = Demo.getObj();
        response.printJson(obj);
    }
}
