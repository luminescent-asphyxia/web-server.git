package com.server.web;

import com.server.web.annotation.WebServlet;
import com.server.web.entity.Entity;
import com.server.web.entity.Mapping;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/27 18:16
 */
public class AnnotationHandler {

    private List<Class<?>> classList;

    private List<Entity> entityList;

    private List<Mapping> mappingList;

    public AnnotationHandler(){
        this.classList = new ArrayList<>();
        this.entityList = new ArrayList<>();
        this.mappingList = new ArrayList<>();
    }

    public void parsePackage(String packageStr) throws Exception {
        if ("".equals(packageStr.trim()) || "com".equals(packageStr)){
            throw new UnsupportedOperationException();
        }
        if (packageStr.contains(".")){
            packageStr = packageStr.replaceAll("[.]","/");
        }
        URL resource = this.getClass().getClassLoader().getResource(packageStr);
        if (resource == null){
            throw new RuntimeException("包路径不存在："+packageStr);
        }
        File file = new File(resource.getFile());
        int cutLen = file.getAbsolutePath().length() - packageStr.length();
        parsePackage(file, cutLen);
        parseAnnotation(classList);
    }

    private void parseAnnotation(List<Class<?>> classList) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, UnsupportedEncodingException {
        for (Class<?> c : classList) {
            Annotation annotation = c.getAnnotation(WebServlet.class);
            if (annotation != null){
                Class<? extends Annotation> aClass = annotation.getClass();
                Method valueMethod = aClass.getMethod("value");
                Method nameMethod = aClass.getMethod("name");
                Method urlPatternsMethod = aClass.getMethod("urlPatterns");
                String[] values = (String[])valueMethod.invoke(annotation);
                String name = (String)nameMethod.invoke(annotation);
                String[] urlPatterns = (String[])urlPatternsMethod.invoke(annotation);
                if (name.length() == 0){
                    name = c.getName();
                }
                if (values.length > 0 && urlPatterns.length > 0) {
                    throw new UnsupportedEncodingException("value and urlPatterns cannot be valid at the same time");
                }
                Set<String> urlSet = new HashSet<>();
                if (values.length > 0){
                    urlSet.addAll(Arrays.asList(values));
                } else if (urlPatterns.length > 0){
                    urlSet.addAll(Arrays.asList(urlPatterns));
                }
                entityList.add(new Entity(name,c.getName()));
                mappingList.add(new Mapping(name,urlSet));
            }
        }
    }

    private void parsePackage(File file,int cutLen){
        File[] files = file.listFiles();
        if (files!=null && files.length!=0){
            for (File f:files){
                parsePackage(f.getAbsoluteFile(),cutLen);
            }
        } else {
            String classStr = file.getAbsolutePath().substring(cutLen);
            if (classStr.contains(".class")){
                classStr = classStr.replace(".class","").replaceAll("\\\\",".");
                try {
                    Class<?> clazz = Class.forName(classStr);
                    classList.add(clazz);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Entity> getEntityList() {
        return entityList;
    }

    public List<Mapping> getMappingList() {
        return mappingList;
    }

}
