package com.server.web;

import com.server.web.entity.Entity;
import com.server.web.entity.Mapping;
import com.server.web.entity.Resources;
import com.server.web.servlet.Servlet;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.net.URL;
import java.util.List;
import java.util.Set;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/8 14:03
 */
public class WebApplication {
    private static WebContext webContext;
    private static Set<Resources> resources;

    public static void init(){
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            WebHandler webHandler = new WebHandler();
            URL resource = Thread.currentThread().getContextClassLoader().
                    getResource(ServerConfig.getWebXmlLocation());
            if (resource == null){
                throw new RuntimeException("web.xml配置文件不存在，请检查路径是否正确："+ServerConfig.getWebXmlLocation());
            }
            saxParser.parse(resource.openStream(), webHandler);
            List<Entity> entityList = webHandler.getServletEntities();
            List<Mapping> mappingList = webHandler.getServletMappingEntities();
            if (ServerConfig.isAnnotationSupport()){
                AnnotationHandler handler = new AnnotationHandler();
                if ("".equals(ServerConfig.getAnnotationPackage())){
                    throw new RuntimeException("您开启了注解配置但未指定注解所在包路径！");
                }
                handler.parsePackage(ServerConfig.getAnnotationPackage());
                entityList.addAll(handler.getEntityList());
                mappingList.addAll(handler.getMappingList());
            }
            webContext = new WebContext(entityList, mappingList);
            resources = webHandler.getResourcesList();
        } catch (Exception e) {
            throw new RuntimeException("解析配置文件错误 "+e.getMessage());
        }
    }

    public static Servlet getServletFromUrl(String url){
        Class<?> clazz = null;
        try {
            clazz = webContext.getClazz(url);
            Servlet servlet = (Servlet) clazz.getConstructor().newInstance();
            return servlet;
        } catch (Exception e) {
            return null;
        }

    }

    public static boolean getResourceFromUrl(String url){
        if ("/".equals(url)){
            return true;
        }
        int lastIndexOf = url.lastIndexOf("/");
        String directory = url.substring(1, lastIndexOf == 0?url.length():lastIndexOf);
        String suffix = "";
        if (url.contains(".")){
            //文件后缀名
            suffix = url.substring(url.lastIndexOf("."));//.png
        }
        for (Resources resource : resources) {
            String dir = resource.getDirectory();
            if (dir.equals(directory)){
                Set<String> patterns = resource.getPatterns();
                if (patterns.contains("/*")){
                    return true;
                }
                for (String pattern : patterns) {
                    //pattern 前面必须加 .
                    if (pattern.equals(suffix)){
                        return true;
                    }
                }
            }
        }
        if (lastIndexOf == 0){
            return false;
        }
        return getResourceFromUrl("/"+directory+suffix);
    }

}
