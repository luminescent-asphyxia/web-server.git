package com.server.web;

import com.server.utils.JsonUtil;
import com.server.utils.SecurityClose;
import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/8 8:33
 */
public class Response {

    public final String BLANK = " ";

    public final String CRLF = "\r\n";

    public final String SEMICOLON = ";";

    private BufferedWriter bw;

    private OutputStream binaryStream;

    private StringBuilder headInfo;

    private StringBuilder content;

    private long len;

    private boolean isError = false;

    private String contentType;

    private String charSetEncoding;

    private static Map<String,String> contentTypeMap = new HashMap<>();

    static {
        contentTypeMap.put(".png","image/png");
        contentTypeMap.put(".jpe","image/jpeg");
        contentTypeMap.put(".jpeg","image/jpeg");
        contentTypeMap.put(".jpg","application/x-jpg");
        contentTypeMap.put(".gif","image/gif");
        contentTypeMap.put(".html","text/html");
        contentTypeMap.put(".mp3","audio/mp3");
        contentTypeMap.put(".mp4","video/mpeg4");
        contentTypeMap.put(".java","java/*");
        contentTypeMap.put(".img","application/x-img");
        contentTypeMap.put(".css","text/css");
    }

    private Response() {
        this.headInfo = new StringBuilder();
        this.content = new StringBuilder();
        this.len = 0;
    }

    public Response(Socket client) {
        this();
        try {
            this.bw = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
            this.binaryStream = client.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
            isError = true;
        }
    }

    public Response(OutputStream os) {
        this();
        this.bw = new BufferedWriter(new OutputStreamWriter(os));
        this.binaryStream = os;
    }

    public Response printJson(Object object) throws IllegalAccessException {
        return this.print(JsonUtil.toJsonStr(object));
    }

    public Response print(String info) {
        content.append(info);
        len += info.getBytes().length;
        return this;
    }

    public Response println(String info) {
        content.append(info).append(CRLF);
        len += info.getBytes().length;
        len += CRLF.getBytes().length;
        return this;
    }

    public Response printHtml(String relativePath){
        this.contentType = "text/html";
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(relativePath)));
            String line = br.readLine();
            while (line != null){
                println(line);
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            SecurityClose.close(br);
        }
        return this;
    }

    public int pushToClientResource(int code,String resourceUri) throws IOException {
        return pushToClient(code,resourceUri);
    }

    public int pushToClient(int code) throws IOException {
        return pushToClient(code, null);
    }

    private int pushToClient(int code,String resourceUri) throws IOException {
        if (isError) {
            code = 500;
        }
        if (resourceUri != null){
            if ("/".equals(resourceUri)){
                resourceUri = "/index.html";
            }
            String suffix;
            try {
                // 文件路径的.可能不存在，意味着它是文件夹或无后缀文件，这种情况直接404
                // StringIndexOutOfBoundsException
                suffix = resourceUri.substring(resourceUri.lastIndexOf("."));
                contentType = contentTypeMap.get(suffix);
                if (contentType == null){
                    contentType = "application/octet-stream";
                }
                URL resource = Thread.currentThread().getContextClassLoader().getResource("static" + resourceUri);
                //resource可能为空，NullPointerException
                InputStream stream = resource.openStream();
                this.len += stream.available();
                createHeadInfo(code);
                bw.append(headInfo);
                bw.flush();
                byte[] bytes = new byte[1024*1024];
                int count = stream.read(bytes);
                while (count != -1){
                    binaryStream.write(bytes,0,count);
                    binaryStream.flush();
                    count = stream.read(bytes);
                }
                return code;
            } catch (Exception e) {
                code = 404;
                contentType = null;
            }
        }
        if (code == 404) {
            printHtml("static/error-pages/404.html");
        }
        if (code == 500){
            this.content = new StringBuilder();
            this.len = 0;
            printHtml("static/error-pages/500.html");
        }
        if (contentType == null) {
            contentType = "text/html";//默认值
        }
        if (charSetEncoding == null) {
            charSetEncoding = "UTF-8";//默认值
        }
        createHeadInfo(code);
        bw.append(headInfo);
        bw.append(content);
        bw.flush();
        return code;
    }

    //构建响应头信息
    private void createHeadInfo(int code) {
        headInfo.append("HTTP/1.1").append(BLANK);
        headInfo.append(code).append(BLANK);
        switch (code) {
            case 200:
                headInfo.append("OK").append(CRLF);
                break;
            case 204:
                headInfo.append("No Content").append(CRLF);
                break;
            case 206:
                headInfo.append("Partial Content").append(CRLF);
                break;
            case 301:
                headInfo.append("Moved Permanently").append(CRLF);
                break;
            case 302:
                headInfo.append("Found").append(CRLF);
                break;
            case 303:
                headInfo.append("See Other").append(CRLF);
                break;
            case 304:
                headInfo.append("Not Modified").append(CRLF);
                break;
            case 307:
                headInfo.append("Temporary Redirect").append(CRLF);
                break;
            case 400:
                headInfo.append("Bad Request").append(CRLF);
                break;
            case 401:
                headInfo.append("Unauthorized").append(CRLF);
                break;
            case 403:
                headInfo.append("Forbidden").append(CRLF);
                break;
            case 404:
                headInfo.append("Not Found").append(CRLF);
                break;
            case 500:
                headInfo.append("Internal Server Error").append(CRLF);
                break;
            case 503:
                headInfo.append("Service Unavailable").append(CRLF);
                break;
        }
        headInfo.append("Date: ").append(getGreenwichDate()).append(CRLF);
        headInfo.append("Server: ").append(Server.serverName).append(CRLF);
        headInfo.append("Content-type: ").append(contentType).append(SEMICOLON).append("charset=").append(charSetEncoding).append(CRLF);
        headInfo.append("Content-Length: ").append(len).append(CRLF);
        headInfo.append(CRLF);
    }

    private String getGreenwichDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat greenwichDate = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);
        greenwichDate.setTimeZone(TimeZone.getTimeZone("GMT"));
        return greenwichDate.format(calendar.getTime());
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setCharSetEncoding(String charSetEncoding) {
        this.charSetEncoding = charSetEncoding;
    }
}
