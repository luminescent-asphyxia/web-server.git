package com.business.logic;

import com.server.web.Request;
import com.server.web.Response;
import com.server.web.servlet.Servlet;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/8 14:36
 */
public class OtherServlet implements Servlet {

    @Override
    public void service(Request request, Response response) {
        response.println("呵呵");
        int a = 10/0;
        response.println("🙂");
    }
}
