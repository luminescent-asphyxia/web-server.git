package com.server.web;

import com.server.utils.logger.LogImpl;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/27 10:19
 */
public class ServerConfig {

    private static int serverPort = 8888;

    private static String webXmlLocation = "web/web.xml";

    private static String annotationPackage = "";

    private static boolean printStaticRequestLog = false;

    private static boolean annotationSupport = true;

    private static int corePoolSize = 5;

    private static int maximumPoolSize = 10;

    private static long keepAliveTime = 20;

    private static TimeUnit timeUnit = TimeUnit.SECONDS;

    private static Map<String,String> initParameters;

    public static void init(){
        initParameters = loadProperties();
        if (initParameters == null){
            return;
        }
        Set<String> keys = initParameters.keySet();
        try {
            for (String key : keys) {
                String value = initParameters.get(key);
                switch (key){
                    case "keepAliveTime":
                        keepAliveTime = Long.parseLong(value);
                        break;
                    case "annotationSupport":
                        annotationSupport = Boolean.parseBoolean(value);
                        break;
                    case "corePoolSize":
                        corePoolSize = Integer.parseInt(value);
                        break;
                    case "printStaticRequestLog":
                        printStaticRequestLog = Boolean.parseBoolean(value);
                        break;
                    case "maximumPoolSize":
                        maximumPoolSize = Integer.parseInt(value);
                        break;
                    case "serverPort":
                        serverPort = Integer.parseInt(value);
                        break;
                    case "webXmlLocation":
                        webXmlLocation = value;
                        break;
                    case "annotationPackage":
                        annotationPackage = value;
                        break;
                    case "timeUnit":
                        value = value.toUpperCase();
                        switch (value){
                            case "NANOSECONDS":
                                timeUnit = TimeUnit.NANOSECONDS;
                                break;
                            case "MICROSECONDS":
                                timeUnit = TimeUnit.MICROSECONDS;
                                break;
                            case "MILLISECONDS":
                                timeUnit = TimeUnit.MILLISECONDS;
                                break;
                            case "SECONDS":
                                timeUnit = TimeUnit.SECONDS;
                                break;
                            case "MINUTES":
                                timeUnit = TimeUnit.MINUTES;
                                break;
                            case "HOURS":
                                timeUnit = TimeUnit.HOURS;
                                break;
                            case "DAYS":
                                timeUnit = TimeUnit.DAYS;
                                break;
                            default:
                                throw new UnsupportedOperationException("不支持的时间单位："+ value);
                        }
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("配置文件读取错误！"+e.getMessage());
        }
    }

    private static Map<String,String> loadProperties(){
        return defaultLoadProperties(null);
    }

    private static Map<String,String> defaultLoadProperties(String propLocation){
        if (propLocation == null || propLocation.trim().length()==0){
            propLocation = "server.properties";
        }
        Properties prop = new Properties();
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propLocation);
        if (inputStream == null){
            LogImpl.getLogger().debug("配置文件server.properties不存在，已启动默认配置");
            return null;
        }
        Map<String,String> configMap = new HashMap();
        try {
            prop.load(inputStream);
            Enumeration<?> enumeration = prop.propertyNames();
            while(enumeration.hasMoreElements()) {
                String key = (String)enumeration.nextElement();
                String value = prop.getProperty(key);
                configMap.put(key, value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return configMap;
    }

    public static int getServerPort() {
        return serverPort;
    }

    public static String getWebXmlLocation() {
        return webXmlLocation;
    }

    public static boolean isPrintStaticRequestLog() {
        return printStaticRequestLog;
    }

    public static boolean isAnnotationSupport() {
        return annotationSupport;
    }

    public static int getCorePoolSize() {
        return corePoolSize;
    }

    public static int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public static long getKeepAliveTime() {
        return keepAliveTime;
    }

    public static TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public static String getAnnotationPackage() {
        return annotationPackage;
    }
}
