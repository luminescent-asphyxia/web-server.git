package com.server.web.servlet;

import com.server.web.Request;
import com.server.web.Response;
import com.server.web.servlet.Servlet;

/**
 * @author qiaoao
 * @description:
 * @date 2021/8/1 14:31
 */
public abstract class GenericServlet implements Servlet {

    public abstract void doGet(Request request, Response response) throws Exception;

    public abstract void doPost(Request request,Response response) throws Exception;

}
