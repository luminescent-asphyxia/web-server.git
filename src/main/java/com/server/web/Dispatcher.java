package com.server.web;

import com.server.utils.SecurityClose;
import com.server.utils.logger.Log;
import com.server.utils.logger.LogImpl;
import com.server.web.servlet.HttpServlet;
import com.server.web.servlet.Servlet;

import java.io.IOException;
import java.net.Socket;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/8 14:45
 */
public class Dispatcher implements Runnable {
    private Socket client;
    private Request request;
    private Response response;

    private Log log = LogImpl.getLogger();

    public Dispatcher(Socket client){
        this.client = client;
    }

    @Override
    public void run() {
        try {
            // 如果是webkit内核浏览器发来的请求，一定发生IO阻塞，影响其他浏览器
            // 这一步必须放在线程里去做，不可以放到构造方法里！
            request = new Request(client);
            response = new Response(client);
        } catch (Exception e) {
            e.printStackTrace();
            this.release();
        }
        if (this.request == null || this.response == null){
            return;
        }
        String url = request.getUrl();
        if (url == null){
            this.release();
            return;
        }
        int code = 200;
        boolean isResource = WebApplication.getResourceFromUrl(url);
        if ("/favicon.ico".equals(url)){
            isResource = true;
        }
        if (isResource){
            try {
                code = response.pushToClientResource(code,url);
            } catch (IOException e) {
                e.printStackTrace();
                code = 500;
            }
        } else {
            Servlet servlet = WebApplication.getServletFromUrl(url);
            try {
                if (servlet == null){
                    code = 404;
                } else {
                    if (servlet instanceof HttpServlet){
                        HttpServlet httpServlet = (HttpServlet)servlet;
                        try {
                            if ("get".equals(request.getMethod())){
                                httpServlet.doGet(request,response);
                            } else if ("post".equals(request.getMethod())){
                                httpServlet.doPost(request,response);
                            } else {
                                response.println("抱歉，暂不支持该请求方式："+request.getMethod().toUpperCase());
                                code = 400;
                            }
                        } catch (UnsupportedOperationException e) {
                            httpServlet.service(request,response);
                        }
                    } else {
                        servlet.service(request,response);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                code = 500;
            }
        }
        try {
            code = response.pushToClient(code);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (ServerConfig.isPrintStaticRequestLog()){
            log.debug("执行了一次请求：uri："+request.getUrl()+" 响应状态码："+code);
        } else {
            if (!isResource)
                log.debug("执行了一次请求：uri："+request.getUrl()+" 响应状态码："+code);
        }
        this.release();
    }
    public void release(){
        SecurityClose.close(this.client);
    }
}
