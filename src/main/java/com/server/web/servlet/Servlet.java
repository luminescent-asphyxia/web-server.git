package com.server.web.servlet;

import com.server.web.Request;
import com.server.web.Response;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/7 16:34
 */
public interface Servlet {
    void service(Request request, Response response) throws Exception;
}
