package com.server.utils.logger;

import org.apache.log4j.Logger;
/**
 * @author qiaoao
 * @description:
 * @date 2021/7/9 20:37
 */
public class LogImpl implements Log {

    private static final Logger logger = Logger.getRootLogger();

    private static LogImpl instance;

    private LogImpl(){}

    public static LogImpl getLogger(){
        if (instance == null){
            synchronized (LogImpl.class){
                if (instance == null){
                    instance = new LogImpl();
                }
            }
        }
        return instance;
    }

    @Override
    public void debug(String message) {
        logger.debug(message);
    }

    @Override
    public void info(String message) {
        logger.info(message);
    }

    @Override
    public void warn(String message) {
        logger.warn(message);
    }

    @Override
    public void error(String message) {
        logger.error(message);
    }

    @Override
    public void fatal(String message) {
        logger.fatal(message);
    }

}
