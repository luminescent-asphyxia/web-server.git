package com.business.logic;

import com.server.web.Request;
import com.server.web.Response;
import com.server.web.servlet.Servlet;
import com.server.web.annotation.WebServlet;
import com.business.logic.entity.Student;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/29 15:52
 */
@WebServlet("/test")
public class TestServlet implements Servlet {
    @Override
    public void service(Request request, Response response) throws Exception {
        response.setContentType("application/json");
        Student stu = new Student();
        stu.setAge(18);
        stu.setId(10001);
        stu.setName("张三");
        List<List<Integer>> list = new ArrayList<>();
        List<Integer> integerList = new ArrayList<>();
        Collections.addAll(integerList,1,2,3);
        List<Integer> integerList2 = new ArrayList<>();
        Collections.addAll(integerList2,4,5,6,7,8,9);
        list.add(integerList);
        list.add(integerList2);
        stu.setItem(list);
        HashMap<Integer, String> map = new HashMap<>();
        map.put(1,"bbb");
        map.put(2,"bbb");
        map.put(3,"bbb");
        map.put(4,"bbb");
        map.put(5,"bbb");
        stu.setHobby(map);
        response.printJson(stu);
    }
}
