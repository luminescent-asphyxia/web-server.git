package com.server;
import com.server.web.Server;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/8 11:30
 */
public class TestMain {
    public static void main(String[] args) {
        Server server = new Server();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            server.stop();
        }));
        server.start();
    }
}
