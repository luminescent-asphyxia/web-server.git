package com.business.logic.entity;

import java.util.HashMap;
import java.util.List;

/**
 * @author qiaoao
 * @description:
 * @date 2021/7/29 16:11
 */
public class Student {
    private String name;
    private Integer age;
    private long id;
    private List<List<Integer>> item;
    private HashMap<Integer, String> hobby;
    private String[] arr;

    public String[] getArr() {
        return arr;
    }

    public void setArr(String[] arr) {
        this.arr = arr;
    }

    public HashMap<Integer, String> getHobby() {
        return hobby;
    }

    public void setHobby(HashMap<Integer, String> hobby) {
        this.hobby = hobby;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public List<List<Integer>> getItem() {
        return item;
    }

    public void setItem(List<List<Integer>> item) {
        this.item = item;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", id=" + id +
                '}';
    }
}
